﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository.Models
{
    public class SuperManager
    {   [Key]     
        public string SMID { get; set; }
        public string Email { get; set; }
    }
}
